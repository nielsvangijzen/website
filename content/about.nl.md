+++
author = "Niels van Gijzen"
title = "Over"
date = "2021-04-01"
description = "Over Mij"
tags = []
+++

Ik studeer forensische informatica aan Hogeschool Leiden, daarnaast
specialiseer ik me in het ethisch hacken.
